<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Historia do BR</title>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    
  </head>
  <body class="d-flex flex-column h-100">
  <header>
  <!-- Fixed navbar -->
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Historia do BR</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <!-- <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li> -->
      </ul>
      <!-- <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form> -->
    </div>
  </nav>
</header>

<!-- Begin page content -->
<main role="main" class="flex-shrink-0">
  <div class="container" id="corpo" style="margin-top: 80px;">
  
<p>A História do Brasil começa com a chegada dos primeiros humanos no continente americano há pelo menos 12 mil anos. Em fins do século XV, quando do Tratado de Tordesilhas, toda a área hoje conhecida como Brasil era habitada por tribos seminômades que subsistiam da caça, pesca, coleta e agricultura. Em 26 de janeiro de 1500, o navegador e explorador espanhol Vicente Yáñez Pinzón atingiu o litoral de Pernambuco, e, aos 22 de abril do mesmo ano, Pedro Álvares Cabral, capitão-mor de expedição portuguesa a caminho das Índias, chegou ao litoral sul da Bahia, tornando a região colônia do Reino de Portugal. Trinta anos depois, a Coroa Portuguesa implementou uma política de colonização para a terra recém-descoberta que se organizou por meio da distribuição de capitanias hereditárias a membros da nobreza, porém esse sistema malogrou, uma vez que somente as capitanias de Pernambuco e São Vicente prosperaram. Em 1548 é criado o Estado do Brasil, com consequente instalação de um governo-geral, e no ano seguinte é fundada a primeira sede colonial, Salvador. A economia da colônia, iniciada com o extrativismo do pau-brasil e as trocas entre os colonos e os índios, gradualmente passou a ser dominada pelo cultivo da cana-de-açúcar para fins de exportação, tendo em Pernambuco o seu principal centro produtor, região que chegou a atingir o posto de maior e mais rica área de produção de açúcar do mundo. No fim do século XVII foram descobertas, através das bandeiras, importantes jazidas de ouro no interior do Brasil que foram determinantes para o seu povoamento e que pontuam o início do chamado Ciclo do Ouro, período que marca a ascensão da Capitania de Minas Gerais — desmembrada da Capitania de São Paulo e Minas de Ouro — na economia colonial. Em 1763, a sede do Estado do Brasil foi transferida para o Rio de Janeiro.

Em 1808, com a transferência da corte portuguesa para o Brasil, fugindo da possível subjugação da França, consequência da Guerra Peninsular travada entre as tropas portuguesas e as de Napoleão Bonaparte, o Príncipe-regente Dom João de Bragança, filho da Rainha Dona Maria I, abriu os portos da então colônia, permitiu o funcionamento de fábricas e fundou o Banco do Brasil. Em 1815, o então Estado do Brasil foi elevado à condição de reino, unido aos reinos de Portugal e Algarves, com a designação oficial de Reino Unido de Portugal, Brasil e Algarves, tendo Dona Maria I de Portugal acumulado as três coroas. Em 7 de setembro de 1822, Dom Pedro de Alcântara proclamou a Independência do Brasil em relação ao Reino Unido de Portugal, Brasil e Algarves, e fundou o Império do Brasil, sendo coroado imperador como Dom Pedro I. Ele reinou até 1831, quando abdicou e passou a Coroa brasileira ao seu filho, Dom Pedro de Alcântara, que tinha apenas cinco anos. Aos catorze anos, em 1840, Dom Pedro de Alcântara (filho) teve sua maioridade declarada, sendo coroado imperador no ano seguinte como Dom Pedro II.

Em 15 de novembro de 1889 ocorreu a Proclamação da República pelo Marechal Deodoro da Fonseca, dando início à chamada República Velha, que só veio a terminar em 1930 com a chegada de Getúlio Vargas ao poder. A partir daí, têm destaque na história brasileira a industrialização do país; sua participação na Segunda Guerra Mundial ao lado dos Estados Unidos; e o Golpe Militar de 1964, quando o general Castelo Branco assumiu a Presidência. A ditadura militar, a pretexto de combater a subversão e a corrupção, suprimiu direitos constitucionais, perseguiu e censurou os meios de comunicação, extinguiu os partidos políticos e criou o bipartidarismo. Após o fim do regime militar, os deputados federais e senadores se reuniram no ano de 1988 em Assembleia Nacional Constituinte e promulgaram a nova Constituição, que amplia os direitos individuais. O país se redemocratiza, avança economicamente e cada vez mais se insere no cenário internacional.
</p><p>testando
</p><p>A História do Brasil começa com a chegada dos primeiros humanos no continente americano há pelo menos 12 mil anos. Em fins do século XV, quando do Tratado de Tordesilhas, toda a área hoje conhecida como Brasil era habitada por tribos seminômades que subsistiam da caça, pesca, coleta e agricultura. Em 26 de janeiro de 1500, o navegador e explorador espanhol Vicente Yáñez Pinzón atingiu o litoral de Pernambuco, e, aos 22 de abril do mesmo ano, Pedro Álvares Cabral, capitão-mor de expedição portuguesa a caminho das Índias, chegou ao litoral sul da Bahia, tornando a região colônia do Reino de Portugal. Trinta anos depois, a Coroa Portuguesa implementou uma política de colonização para a terra recém-descoberta que se organizou por meio da distribuição de capitanias hereditárias a membros da nobreza, porém esse sistema malogrou, uma vez que somente as capitanias de Pernambuco e São Vicente prosperaram. Em 1548 é criado o Estado do Brasil, com consequente instalação de um governo-geral, e no ano seguinte é fundada a primeira sede colonial, Salvador. A economia da colônia, iniciada com o extrativismo do pau-brasil e as trocas entre os colonos e os índios, gradualmente passou a ser dominada pelo cultivo da cana-de-açúcar para fins de exportação, tendo em Pernambuco o seu principal centro produtor, região que chegou a atingir o posto de maior e mais rica área de produção de açúcar do mundo. No fim do século XVII foram descobertas, através das bandeiras, importantes jazidas de ouro no interior do Brasil que foram determinantes para o seu povoamento e que pontuam o início do chamado Ciclo do Ouro, período que marca a ascensão da Capitania de Minas Gerais — desmembrada da Capitania de São Paulo e Minas de Ouro — na economia colonial. Em 1763, a sede do Estado do Brasil foi transferida para o Rio de Janeiro.

Em 1808, com a transferência da corte portuguesa para o Brasil, fugindo da possível subjugação da França, consequência da Guerra Peninsular travada entre as tropas portuguesas e as de Napoleão Bonaparte, o Príncipe-regente Dom João de Bragança, filho da Rainha Dona Maria I, abriu os portos da então colônia, permitiu o funcionamento de fábricas e fundou o Banco do Brasil. Em 1815, o então Estado do Brasil foi elevado à condição de reino, unido aos reinos de Portugal e Algarves, com a designação oficial de Reino Unido de Portugal, Brasil e Algarves, tendo Dona Maria I de Portugal acumulado as três coroas. Em 7 de setembro de 1822, Dom Pedro de Alcântara proclamou a Independência do Brasil em relação ao Reino Unido de Portugal, Brasil e Algarves, e fundou o Império do Brasil, sendo coroado imperador como Dom Pedro I. Ele reinou até 1831, quando abdicou e passou a Coroa brasileira ao seu filho, Dom Pedro de Alcântara, que tinha apenas cinco anos. Aos catorze anos, em 1840, Dom Pedro de Alcântara (filho) teve sua maioridade declarada, sendo coroado imperador no ano seguinte como Dom Pedro II.

Em 15 de novembro de 1889 ocorreu a Proclamação da República pelo Marechal Deodoro da Fonseca, dando início à chamada República Velha, que só veio a terminar em 1930 com a chegada de Getúlio Vargas ao poder. A partir daí, têm destaque na história brasileira a industrialização do país; sua participação na Segunda Guerra Mundial ao lado dos Estados Unidos; e o Golpe Militar de 1964, quando o general Castelo Branco assumiu a Presidência. A ditadura militar, a pretexto de combater a subversão e a corrupção, suprimiu direitos constitucionais, perseguiu e censurou os meios de comunicação, extinguiu os partidos políticos e criou o bipartidarismo. Após o fim do regime militar, os deputados federais e senadores se reuniram no ano de 1988 em Assembleia Nacional Constituinte e promulgaram a nova Constituição, que amplia os direitos individuais. O país se redemocratiza, avança economicamente e cada vez mais se insere no cenário internacional.
</p><p>A História do Brasil começa com a chegada dos primeiros humanos no continente americano há pelo menos 12 mil anos. Em fins do século XV, quando do Tratado de Tordesilhas, toda a área hoje conhecida como Brasil era habitada por tribos seminômades que subsistiam da caça, pesca, coleta e agricultura. Em 26 de janeiro de 1500, o navegador e explorador espanhol Vicente Yáñez Pinzón atingiu o litoral de Pernambuco, e, aos 22 de abril do mesmo ano, Pedro Álvares Cabral, capitão-mor de expedição portuguesa a caminho das Índias, chegou ao litoral sul da Bahia, tornando a região colônia do Reino de Portugal. Trinta anos depois, a Coroa Portuguesa implementou uma política de colonização para a terra recém-descoberta que se organizou por meio da distribuição de capitanias hereditárias a membros da nobreza, porém esse sistema malogrou, uma vez que somente as capitanias de Pernambuco e São Vicente prosperaram. Em 1548 é criado o Estado do Brasil, com consequente instalação de um governo-geral, e no ano seguinte é fundada a primeira sede colonial, Salvador. A economia da colônia, iniciada com o extrativismo do pau-brasil e as trocas entre os colonos e os índios, gradualmente passou a ser dominada pelo cultivo da cana-de-açúcar para fins de exportação, tendo em Pernambuco o seu principal centro produtor, região que chegou a atingir o posto de maior e mais rica área de produção de açúcar do mundo. No fim do século XVII foram descobertas, através das bandeiras, importantes jazidas de ouro no interior do Brasil que foram determinantes para o seu povoamento e que pontuam o início do chamado Ciclo do Ouro, período que marca a ascensão da Capitania de Minas Gerais — desmembrada da Capitania de São Paulo e Minas de Ouro — na economia colonial. Em 1763, a sede do Estado do Brasil foi transferida para o Rio de Janeiro.

Em 1808, com a transferência da corte portuguesa para o Brasil, fugindo da possível subjugação da França, consequência da Guerra Peninsular travada entre as tropas portuguesas e as de Napoleão Bonaparte, o Príncipe-regente Dom João de Bragança, filho da Rainha Dona Maria I, abriu os portos da então colônia, permitiu o funcionamento de fábricas e fundou o Banco do Brasil. Em 1815, o então Estado do Brasil foi elevado à condição de reino, unido aos reinos de Portugal e Algarves, com a designação oficial de Reino Unido de Portugal, Brasil e Algarves, tendo Dona Maria I de Portugal acumulado as três coroas. Em 7 de setembro de 1822, Dom Pedro de Alcântara proclamou a Independência do Brasil em relação ao Reino Unido de Portugal, Brasil e Algarves, e fundou o Império do Brasil, sendo coroado imperador como Dom Pedro I. Ele reinou até 1831, quando abdicou e passou a Coroa brasileira ao seu filho, Dom Pedro de Alcântara, que tinha apenas cinco anos. Aos catorze anos, em 1840, Dom Pedro de Alcântara (filho) teve sua maioridade declarada, sendo coroado imperador no ano seguinte como Dom Pedro II.

Em 15 de novembro de 1889 ocorreu a Proclamação da República pelo Marechal Deodoro da Fonseca, dando início à chamada República Velha, que só veio a terminar em 1930 com a chegada de Getúlio Vargas ao poder. A partir daí, têm destaque na história brasileira a industrialização do país; sua participação na Segunda Guerra Mundial ao lado dos Estados Unidos; e o Golpe Militar de 1964, quando o general Castelo Branco assumiu a Presidência. A ditadura militar, a pretexto de combater a subversão e a corrupção, suprimiu direitos constitucionais, perseguiu e censurou os meios de comunicação, extinguiu os partidos políticos e criou o bipartidarismo. Após o fim do regime militar, os deputados federais e senadores se reuniram no ano de 1988 em Assembleia Nacional Constituinte e promulgaram a nova Constituição, que amplia os direitos individuais. O país se redemocratiza, avança economicamente e cada vez mais se insere no cenário internacional.
</p><p style="visibility: hidden">$texto</p>
  </div>
</main>

<!-- <footer class="footer mt-auto py-3">
  <div class="container">
    <span class="text-muted">Place sticky footer content here.</span>
  </div>
</footer> -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/docs/4.2/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="/docs/4.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-zDnhMsjVZfS3hiP7oCBRmfjkQC4fzxVxFhBx8Hkz2aZX8gEvA/jsP3eXRCvzTofP" crossorigin="anonymous"></script>
</body>

</html>

