/**
 * Define a grammar called CEW
 */
grammar GrammarDSL;


init : action SEMI folder_name ('/' init)* | action SEMI folder_name | texto | json | html | jsp | xml | jpg;

texto : TXTFILE ;

json : JSONFILE ;

html : HTMLFILE ;

jsp : JSPFILE ;

xml : XMLFILE ;

jpg : JPGFILE;

action:
	Publish | Download | Upload;
	
folder_name :
	STRING;

Publish :
	'publish';
	
Download :
	'download';
	
Upload:
	'upload';

SEMI : 
	' ; ';

STRING : 
	'"' .*? '"' ;
	
TXTFILE :
	.*? '.txt' ;
	
JSONFILE :
	 .*? '.json' ;
	 
HTMLFILE :
 	 .*? '.html' ;
 	 
JSPFILE :
	.*? '.jsp' ;
	
JPGFILE :
	.*? '.jpg';
	
XMLFILE :
	.*? '.xml' -> skip ;

WS : 
	[ \t\r\n]+ -> skip ;
