package server;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public final class WebManager {
	
	public void attachText(String textToBeAttached, String filePath)
	{
		try {
			File htmlTemplateFile = getFile(filePath);
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String nextEmptyHost = "<p style=\"visibility: hidden\">$texto</p>";
			String body = "<p>" + textToBeAttached + "</p>";
			htmlString = htmlString.replace("<p style=\"visibility: hidden\">$texto</p>", body + nextEmptyHost);
			File newHtmlFile = new File(filePath);
			FileUtils.writeStringToFile(newHtmlFile, htmlString);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("deprecation")
	public void attachImage(String imageToBeAttached, String filePath)
	{
		try {
			File htmlTemplateFile = getFile(filePath);
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String nextEmptyHost = "<p style=\"visibility: hidden\">$texto</p>";
			String[] fileName = imageToBeAttached.split("/");
			String[] caption = fileName[fileName.length - 1].split("\\.");
			String body = "<p><figure><img width='450px' src=\'images/" + escapeHtml4(fileName[fileName.length - 1]) + "\' /><figcaption>" + caption[0].replaceAll("-", " ") + "</figcapton></figure></p>";
			htmlString = htmlString.replace("<p style=\"visibility: hidden\">$texto</p>", body + nextEmptyHost);
			File newHtmlFile = new File(filePath);
			FileUtils.writeStringToFile(newHtmlFile, htmlString);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	public void attachUploadedFile(String fileToBeAttached, String filePath)
	{
		try {
			File htmlTemplateFile = getFile(filePath);
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String nextEmptyHost = "<p style=\"visibility: hidden\">$texto</p>";
			String[] fileName = fileToBeAttached.split("/");
			String body = "<p>"+ fileName[fileName.length - 1] +" (<a href=\'uploaded_files/" + escapeHtml4(fileName[fileName.length - 1]) + "\' />Download</a>)</p>";
			htmlString = htmlString.replace("<p style=\"visibility: hidden\">$texto</p>", body + nextEmptyHost);
			File newHtmlFile = new File(filePath);
			FileUtils.writeStringToFile(newHtmlFile, htmlString);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void createHtml(String name, String webAppPath) {
		try {
			File htmlTemplateFile = getFile(webAppPath + name.replace(" ", "-").replaceAll("^\"|\"$", "") + ".jsp");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String body = name;
			htmlString = htmlString.replace("$nome", body.replaceAll("\"", ""));
			File newHtmlFile = new File(webAppPath + name.replace(" ", "-").replaceAll("^\"|\"$", "") + ".jsp");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);
			addLinkToPage(newHtmlFile, webAppPath, name.replaceAll("^\"|\"$", ""));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public File getFile(String filePath)
	{
		
		try {
			File file = new File(filePath);
			if(file.exists() && !file.isDirectory()) {
				return file;
			} else {
				return new File("/Users/rafael/dsl-maven/dslweb/src/main/webapp/template.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public void addLinkToPage(File filePath, String webAppPath, String name)
	{
		try {
			File index = new File(webAppPath + "index.jsp");
			String htmlString = FileUtils.readFileToString(index);
			
			String Host = "<li class=\"nav-item\"><a class=\"nav-link\" href=\"" + name.replace(" ", "-").replaceAll("^\"|\"$", "") + ".jsp" +"\">" + name.replaceAll("^\"|\"$", "") + "</a></li>";
			if(htmlString.contains(Host)) return;
			
			String nextEmptyHost = "<li class=\"nav-item\" style=\"visibility: hidden\"><a class=\"nav-link\" href=\"#\">$link</a></li>";
			htmlString = htmlString.replace("<li class=\"nav-item\" style=\"visibility: hidden\"><a class=\"nav-link\" href=\"#\">$link</a></li>", "<li class=\"nav-item\"><a class=\"nav-link\" href=\""+ name.replaceAll(" ", "-") + ".jsp" + "\">" + name + "</a></li>" + nextEmptyHost);
			FileUtils.writeStringToFile(index, htmlString);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
}
