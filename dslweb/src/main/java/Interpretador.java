import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import server.WebManager;

class Interpretador extends GrammarDSLBaseListener {
	
	String myPath;
	String indexJspPath = "/Users/rafael/dsl-maven/dslweb/src/main/webapp/index.jsp";
	String templateJspPath = "/Users/rafael/dsl-maven/dslweb/src/main/webapp/template.jsp";
	String webAppPath = "/Users/rafael/dsl-maven/dslweb/src/main/webapp/";
	String imgPath = "/Users/rafael/Development/Java/apache-tomcat-8.5.38/webapps/images/";
	String uploadedFilesPath = "/Users/rafael/Development/Java/apache-tomcat-8.5.38/webapps/uploaded_files/";
	String publishPath;
	
	public Interpretador(String path) {
		this.myPath = path;
	}

	@Override
	public void enterInit(GrammarDSLParser.InitContext ctx) {
		super.enterInit(ctx);
		if (ctx.action() != null && ctx.folder_name() != null) {
//			System.out.println(ctx.action().getText());
//			System.out.println(ctx.folder_name().getText());
		}
	}

	
	@Override
	public void enterJson(GrammarDSLParser.JsonContext ctx) {
		super.enterJson(ctx);
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(this.myPath));
  		    StringBuilder sb = new StringBuilder();
  		    String line = br.readLine();

  		    while (line != null) {
  		        sb.append(line);
  		        sb.append(System.lineSeparator());
  		        line = br.readLine();
  		    }
  		    
  		    String everything = sb.toString();
  		     
  		    JSONParser parser = new JSONParser();
  		    JSONObject jObject = (JSONObject)parser.parse(everything);
  		    if(jObject.containsKey("texto"))
  		    {
  		    	String texto = jObject.get("texto").toString();
  		    	String folder_name = jObject.get("folder_name").toString().replaceAll(" ", "-").concat(".jsp");
  	  		    
  	  		    WebManager wm = new WebManager();
  	  		    wm.attachText(texto, webAppPath + folder_name);
  		    } else if (jObject.containsKey("nome"))
  		    {
  		    	String nome = jObject.get("nome").toString();
  		    	
  		    	WebManager wm = new WebManager();
  		    	wm.createHtml(nome, this.webAppPath);
  		    } else if (jObject.containsKey("img")) {
  		    	String img = jObject.get("img").toString();
  		    	String folder_name = jObject.get("folder_name").toString().replaceAll(" ", "-").concat(".jsp");
  		    	
  		    	WebManager wm = new WebManager();
  		    	wm.attachImage(imgPath + img.replaceAll(" ", "-"), webAppPath + folder_name);
  		    } else if (jObject.containsKey("uploaded_files")) {
  		    	String uploaded_files = jObject.get("uploaded_files").toString();
  		    	String folder_name = jObject.get("folder_name").toString().replaceAll(" ", "-").concat(".jsp");
  		    	
  		    	WebManager wm = new WebManager();
  		    	wm.attachUploadedFile(uploadedFilesPath + uploaded_files.replaceAll(" ", "-"), webAppPath + folder_name);
  		    }
  		    	
  		    br.close();
  		    
  		    
  		   
  		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void enterJpg(GrammarDSLParser.JpgContext ctx) {
		super.enterJpg(ctx);
		
//		try {
//			  		    
//  		    WebManager wm = new WebManager();
//  		    wm.attachImage(this.myPath, this.indexJspPath);
//  		    
//  		    
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
	}
	
}