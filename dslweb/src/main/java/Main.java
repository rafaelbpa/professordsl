import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.WatchEvent.Kind;


public class Main {

	private static String ROOT_WATCH = "/Users/rafael/dsl-maven/dslweb/src/main/webapp/publish ; \"json\"";
	
	public static void main(String[] args) throws Exception {
		System.out.println("=== Iniciando projeto remoto ===");
		File dir = new File(args[0]);
		System.out.println("LOCAL = " + args[0]);
//		new File(args[0] + "/dsl").mkdirs();
		
		while (true)
		{
			watchDirectoryPath(dir.toPath());
		}
		
		
		
		//LeitorDePasta(dir.toString());
	}
	
	private static void LeitorDePasta(String caminhoPasta)
	{
		File dir = new File(caminhoPasta);
		File files[] = dir.listFiles();
		if (files == null) return;
		for(int i = 0 ; i < files.length ; i++)
		{
			if(!".DS_Store".equals(dir.list()[i].toString()) && !"WEB-INF".equals(dir.list()[i].toString()) && !"images".equals(dir.list()[i].toString()) && !"uploaded_files".equals(dir.list()[i].toString()))
			{
				String path[] = files[i].toString().split(caminhoPasta + "/");
				if(files[i].isDirectory())
				{
					LeitorDsl.DslParser(path[1], caminhoPasta + "/" + path[1]);
					//LeitorDePasta(files[i].toString());
				}
				else
				{
					LeitorDsl.DslParser(path[1], caminhoPasta + "/" + path[1]);
				}
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public static void watchDirectoryPath(Path path) {
        // Sanity check - Check if path is a folder
        try {
            Boolean isFolder = (Boolean) Files.getAttribute(path,
                    "basic:isDirectory", NOFOLLOW_LINKS);
            if (!isFolder) {
                throw new IllegalArgumentException("Path: " + path
                        + " is not a folder");
            }
        } catch (IOException ioe) {
            // Folder does not exists
            ioe.printStackTrace();
        }

        System.out.println("Watching path: " + path);

        // We obtain the file system of the Path
        FileSystem fs = path.getFileSystem();

        // We create the new WatchService using the new try() block
        try (WatchService service = fs.newWatchService()) {

            // We register the path to the service
            // We watch for creation events
        	path.register(service, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE); 

            // Start the infinite polling loop
            WatchKey key = null;
            while (true) {
                key = service.take();

                // Dequeueing events
                Kind<?> kind = null;
                for (WatchEvent<?> watchEvent : key.pollEvents()) {
                    // Get the type of the event
                    kind = watchEvent.kind();
                    if (OVERFLOW == kind) {
                        continue; // loop
                    } else if (ENTRY_CREATE == kind) {
                        // A new Path was created
                        Path newPath = ((WatchEvent<Path>) watchEvent)
                                .context();
                        // Output
                        System.out.println("New path created: " + newPath);
                        if(!".DS_Store".equals(newPath.toString()))
                        {
                        	LeitorDePasta(path.toString() + "/" + newPath.toString());
                        	//LeitorDePasta(ROOT_WATCH);
                        }
                    } else if (ENTRY_MODIFY == kind) {
                        // modified
                        Path newPath = ((WatchEvent<Path>) watchEvent)
                                .context();
                        // Output
                        System.out.println("New path modified: " + newPath);
                        if(!".DS_Store".equals(newPath.toString()))
                        {
                        	LeitorDePasta(path.toString() + "/" + newPath.toString());
                        	//LeitorDePasta(ROOT_WATCH);
                        }
                    }
                }

                if (!key.reset()) {
                    break; // loop
                }
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }

    }

}
