grammar GrammarDSL;


init : action SEMI folder_name ('/' init)* | action SEMI folder_name | texto | json | html | jsp | xml | jpg | pptx;

texto : TXTFILE ;

json : JSONFILE ;

html : HTMLFILE ;

jsp : JSPFILE ;

xml : XMLFILE ;

jpg : JPGFILE;

pptx : PPTXFILE;

action:
	Publish | Download | Upload;
	
folder_name :
	STRING;

Publish :
	'publish';
	
Download :
	'download';
	
Upload:
	'upload';

SEMI : 
	' ; ';

STRING : 
	'"' .*? '"' ;
	
TXTFILE :
	.*? '.txt' ;
	
JSONFILE :
	 .*? '.json' ;
	 
HTMLFILE :
 	 .*? '.html' ;
 	 
JSPFILE :
	.*? '.jsp' ;
	
JPGFILE :
	.*? '.jpg';
	
PPTXFILE :
	.*? '.pptx';
	
XMLFILE :
	.*? '.xml' -> skip ;

WS : 
	[ \t\r\n]+ -> skip ;
