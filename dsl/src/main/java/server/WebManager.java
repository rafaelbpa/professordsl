package server;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public final class WebManager {
	private static WebManager INSTANCE;
	private static String indexPath;
	
	public static WebManager getInstance(String filePath)
	{
		if(INSTANCE == null) {
			indexPath = filePath;
			INSTANCE = new WebManager();
			return INSTANCE;
		} else {
			return INSTANCE;
		}
	}
	
	public void attachText(String textToBeAttached, String filePath)
	{
		try {
			File htmlTemplateFile = new File(filePath);
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String nextEmptyHost = "<p style=\"visibility: hidden\">$texto</p>";
			String body = "<p>" + textToBeAttached + "</p>";
			htmlString = htmlString.replace("<p style=\"visibility: hidden\">$texto</p>", body + nextEmptyHost);
			File newHtmlFile = new File(filePath);
			FileUtils.writeStringToFile(newHtmlFile, htmlString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
