import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CodePointCharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;


public class LeitorDsl {
	
	public static void DslParser(String minhaString, String completePath)
	{
		//ANTLRInputStream input = new ANTLRInputStream(minhaString);
		CodePointCharStream input = CharStreams.fromString(minhaString);
		System.out.println(minhaString);
		GrammarDSLLexer lexer = new GrammarDSLLexer(input);
		
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		
		GrammarDSLParser parser = new GrammarDSLParser(tokens);
		
		ParseTree tree = parser.init();
		
		System.out.println(tree.toStringTree(parser));
		
		ParseTreeWalker walker = new ParseTreeWalker();
		
		walker.walk(new Interpretador(completePath), tree);
	}
}
