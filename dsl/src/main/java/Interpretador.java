import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;

class Interpretador extends GrammarDSLBaseListener {
	
	String myPath;
	String indexJspPath = "/Users/rafael/dsl-maven/dslweb/src/main/webapp/index.jsp";
	String webAppPath = "/Users/rafael/dsl-maven/dslweb/src/main/webapp";
	String imagePath = "/Users/rafael/Development/Java/apache-tomcat-8.5.38/webapps/images/";
	String uploadedFilesPath = "/Users/rafael/Development/Java/apache-tomcat-8.5.38/webapps/uploaded_files/";
	String publishPath;
	
	public Interpretador(String path) {
		this.myPath = path;
	}

	@Override
	public void enterInit(GrammarDSLParser.InitContext ctx) {
		super.enterInit(ctx);
		if (ctx.action() != null && ctx.folder_name() != null) {
			if(ctx.action().children.get(0).toString().equals("publish")) {
				createHtml(ctx.folder_name().children.get(0).toString());
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
  	public void enterTexto(GrammarDSLParser.TextoContext ctx) {
  		super.enterTexto(ctx);

  		try {
  			BufferedReader br = new BufferedReader(new FileReader(this.myPath));
  		    StringBuilder sb = new StringBuilder();
  		    String line = br.readLine();

  		    while (line != null) {
  		        sb.append(line);
  		        sb.append(System.lineSeparator());
  		        line = br.readLine();
  		    }
  		    
  		    String[] fileName = this.myPath.split("/");
  		    String tmp = fileName[fileName.length - 2];
			String[] metadata = tmp.split(";");
  		    
  		    String everything = sb.toString();
  		    JSONObject jObject = new JSONObject();
  		    jObject.put("texto", everything);
  		    jObject.put("action", metadata[0].trim());
		    jObject.put("folder_name", metadata[1].trim().replaceAll("^\"|\"$", ""));
  		 
  		    br.close();
  		    // TODO subir 
  		    System.out.println(jObject);
  		    List<String> lines = Arrays.asList(jObject.toJSONString());
		    Path file = Paths.get(this.webAppPath + "/publish ; \"json\"/" + new Date().getTime() + ".json");
		    Files.write(file, lines, Charset.forName("UTF-8"));
  		  	
  		   
  		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
  	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void enterJpg(GrammarDSLParser.JpgContext ctx) {
		super.enterJpg(ctx);
		
		try {
			String[] fileName = this.myPath.split("/");
			FileUtils.copyFile(new File(this.myPath), new File(this.imagePath + fileName[fileName.length - 1].replaceAll(" ", "-")));
			FileUtils.copyFile(new File(this.myPath), new File(this.webAppPath + "/images/" + fileName[fileName.length - 1].replaceAll(" ", "-")));
			
			String tmp = fileName[fileName.length - 2];
			String[] metadata = tmp.split(";");
			
  		    JSONObject jObject = new JSONObject();
  		    jObject.put("img", fileName[fileName.length - 1]);
  		    jObject.put("action", metadata[0].trim());
  		    jObject.put("folder_name", metadata[1].trim().replaceAll("^\"|\"$", ""));
  		 
  		    List<String> lines = Arrays.asList(jObject.toJSONString());
		    Path file = Paths.get(this.webAppPath + "/publish ; \"json\"/" + new Date().getTime() + ".json");
		    Files.write(file, lines, Charset.forName("UTF-8"));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void enterAction(GrammarDSLParser.ActionContext ctx) {
		super.enterAction(ctx);
		System.out.println("myPath => "  + this.myPath);
	}
	
	@Override
	public void enterJsp(GrammarDSLParser.JspContext ctx) {
		super.enterJsp(ctx);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void enterPptx(GrammarDSLParser.PptxContext ctx) {
		super.enterPptx(ctx);
		System.out.println("PPTX => "  + this.myPath);
		try {
			String[] fileName = this.myPath.split("/");
			FileUtils.copyFile(new File(this.myPath), new File(this.uploadedFilesPath + fileName[fileName.length - 1].replaceAll(" ", "-")));
			FileUtils.copyFile(new File(this.myPath), new File(this.webAppPath + "/uploaded_files/" + fileName[fileName.length - 1].replaceAll(" ", "-")));
		
		
			String tmp = fileName[fileName.length - 2];
			String[] metadata = tmp.split(";");
			
  		    JSONObject jObject = new JSONObject();
  		    jObject.put("uploaded_files", fileName[fileName.length - 1]);
  		    jObject.put("action", metadata[0].trim());
  		    jObject.put("folder_name", metadata[1].trim().replaceAll("^\"|\"$", ""));
  		 
  		    List<String> lines = Arrays.asList(jObject.toJSONString());
		    Path file = Paths.get(this.webAppPath + "/publish ; \"json\"/" + new Date().getTime() + ".json");
		    Files.write(file, lines, Charset.forName("UTF-8"));
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void createHtml(String name) {
		try {
			String[] fileName = this.myPath.split("/");
  		    String tmp = fileName[fileName.length - 1];
			String[] metadata = tmp.split(";");
			
			String everything = name;
  		    JSONObject jObject = new JSONObject();
  		    jObject.put("nome", everything);
  		    jObject.put("action", metadata[0].trim());
		    jObject.put("folder_name", metadata[1].trim().replaceAll("^\"|\"$", ""));
  		 
  		    List<String> lines = Arrays.asList(jObject.toJSONString());
		    Path file = Paths.get(this.webAppPath + "/publish ; \"json\"/" + new Date().getTime() + ".json");
		    Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}